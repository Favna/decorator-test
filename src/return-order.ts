const log = (msg: string) => console.log(`${msg}\n`);

function Specs(numGears: number, numWheels: number) {
  log('-- decorator function invoked --');

  return function extend <T extends { new(...args: any[]): {} }>(constructor: T) {
    log('-- about to return class extends constructor --');

    return class extends constructor {
      gears = numGears;
      wheels = numWheels;
    };
  };
}

class Wagon {
  public make: string;
  constructor(make: string) {
    log('-- Wagon constructor invoked --');
    this.make = make;
  }
}

@Specs(4, 2)
class Bike extends Wagon {
  public color: string;
  private numOfWheels: number;
  constructor(make: string, color: string) {
    log('--- Bike constructor invoked ---');
    super(make);
    this.color = color;
    this.numOfWheels = 2;
  }
}

@Specs(6, 4)
class Car extends Wagon {
  public numOfSeats: number;
  private numOfWheels: number;
  constructor(make: string, numOfSeats: number) {
    log('--- Car constructor invoked ---');
    super(make);
    this.numOfSeats = numOfSeats;
    this.numOfWheels = 4;
  }
}

log('-- creating an instance of Car --');
const car = new Car('Nissan', 5);
log(`numberOfWheels on car: ${car['numOfWheels']}`);
log(`number of seats on car: ${car['numOfWheels']}`);

log('-- creating an instance of Bike --');
const coolBike = new Bike('CoolBike', 'Red');
log(`numberOfWheels on coolBike: ${coolBike['numOfWheels']}`);
log(`colour on coolBike: ${coolBike.color}`);